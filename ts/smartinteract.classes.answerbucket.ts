import * as plugins from './smartinteract.plugins.js';
import { type IAnswerObject } from './smartinteract.classes.smartinteract.js';

/**
 * class AnswerBucket holds answers
 */
export class AnswerBucket {
  private answerMap = new plugins.lik.ObjectMap<IAnswerObject>();

  /**
   * add an answer to the bucket
   */
  public addAnswer(answerArg: IAnswerObject) {
    this.answerMap.add(answerArg);
  }

  /**
   * gets an answer for a specific name
   */
  public getAnswerFor(nameArg: string) {
    const answer = this.answerMap.findSync((answerArg) => {
      return answerArg.name === nameArg;
    });
    return answer ? answer.value : null;
  }

  /**
   * gets all answers as array
   */
  public getAllAnswers() {
    return this.answerMap.getArray();
  }
}
