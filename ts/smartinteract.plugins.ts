// pushrocks scope
import * as lik from '@push.rocks/lik';
import * as smartobject from '@push.rocks/smartobject';
import * as smartpromise from '@push.rocks/smartpromise';

export { lik, smartobject, smartpromise };

// third party scope
import * as inquirer from 'inquirer';

export { inquirer };
